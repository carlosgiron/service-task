# service-task
Prueba de mantenimiento de tareas

## Descargar el proyecto desde el repositorio
```
https://gitlab.com/carlosgiron/service-task.git
```
### Consideraciones
- Abrir en su editor de preferencia(visual code, IntelliJ, springtools)
- Tener configurado maven
- Tener instalado java
- Tener instalado docker

## Creacion de la base de datos
- Crear base de datos en Mysql

```
create database bd_task;
```
- Usar los datos de autenticación de la base de datos y configurar el application.yml

```
spring:
  application:
    name: api-task
  datasource:
    url: jdbc:mysql://{host}:{port}/bd_task
    username: {tuusuario}
    password: {tucontraseña}
```
## Instalacion
```
mvnw install
mvnw package
```
## Ejecución
```
java -jar target/service-task-1.0.jar
```
## Docker
```
docker build -t api-task .
docker run -p 8080:8080 api-task
```
## Docker Compose
```
docker-compose up -d
```
## Swagger
* [api-docs](http://localhost:8080/v3/api-docs/)
* [swagger](http://localhost:8080/swagger-ui/index.html)

