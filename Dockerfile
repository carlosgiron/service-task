

# the second stage of our build will use open jdk 8 on alpine 3.9
FROM openjdk:11
# copy only the artifacts we need from the first stage and discard the rest

ADD ./target/service-task-1.0.jar service-task-1.0.jar

# set the startup command to execute the jar
CMD ["java", "-jar", "service-task-1.0.jar"]
