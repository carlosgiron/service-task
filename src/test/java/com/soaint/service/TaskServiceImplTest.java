package com.soaint.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import com.soaint.domain.Task;
import com.soaint.exception.TaskNotFoundException;
import com.soaint.repository.TaskRepository;
import com.soaint.service.impl.TaskServiceImpl;
import com.soaint.service.request.TaskRequest;
import com.soaint.service.response.BaseResponse;
import com.soaint.service.response.TaskResponse;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.constraints.Null;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TaskServiceImplTest {
  @Mock
  private TaskRepository taskRepository;
  @InjectMocks
  private TaskServiceImpl taskService;

  Task task;
  TaskRequest taskRequest;
  TaskResponse taskResponse;
  @BeforeEach
  void setUp() {
    task = new Task();
    task.setIdTask(1l);
    task.setDescripcion("tarea 1");
    task.setFechaCreacion(new Date());
    task.setFechaModificacion(null);
    task.setVigente(true);


    taskRequest = new TaskRequest();
    taskRequest.setDescripcion("Test 1");
    taskRequest.setVigente(true);

    taskResponse = new TaskResponse();
    taskResponse.setIdTask(1l);
    taskResponse.setDescripcion("Test 1");
    taskResponse.setVigente(true);
  }

  @Test
  void getAll() {
    when(taskRepository.findAll()).thenReturn(Collections.singletonList(task));
    System.out.println(task);
    List<TaskResponse> response = taskService.getAll();
    assertThat(response.size()).isNotZero();
  }

  @Test
  void getAllEmpty() {
    when(taskRepository.findAll()).thenReturn(Collections.emptyList());
    List<TaskResponse> response = taskService.getAll();
    assertThat(response.size()).isZero();
  }

  @Test
  void getAllCath() {
    when(taskRepository.findAll()).thenReturn(null);
    Throwable exception = assertThrows(
        NullPointerException.class, () -> {
          taskService.getAll();
          throw new NullPointerException("null");
        }
    );

    assertEquals("null", exception.getMessage());
  }

  @Test
  void addTask() {
    BaseResponse response = taskService.addTask(taskRequest);
    assertEquals(response.getCodigoRespuesta(), "01");
  }

  @Test
  void addTaskNull() {
    Throwable exception = assertThrows(
        NullPointerException.class, () -> {
          taskService.addTask(null);
          throw new NullPointerException("null");
        }
    );
    assertEquals("null", exception.getMessage());
  }

  @Test
  void editTask() {
    when(taskRepository.findById(1l)).thenReturn(Optional.ofNullable(task));
    BaseResponse response = taskService.editTask(taskRequest, 1l);
    assertEquals(response.getCodigoRespuesta(), "01");
  }

  @Test
  void editTaskNotException() {
    Throwable exception = assertThrows(
        TaskNotFoundException.class, () -> {
          taskService.editTask(taskRequest, 1l);
        }
    );
    assertEquals(exception.getMessage(), "Error Tarea no disponible");
  }

  @Test
  void editTaskException() {

    Throwable exception = assertThrows(
        NullPointerException.class, () -> {
          when(taskRepository.findById(null)).thenReturn(null);
          taskService.editTask(null, null);
          throw new NullPointerException("null");
        }
    );
    assertEquals(exception.getMessage(), "null");
  }

  @Test
  void deleteTask() {
    when(taskRepository.findById(1l)).thenReturn(Optional.ofNullable(task));
    BaseResponse response = taskService.deleteTask(1l);
    assertEquals(response.getCodigoRespuesta(), "01");
  }

  @Test
  void deleteTaskNotException() {
    Throwable exception = assertThrows(
        TaskNotFoundException.class, () -> {
          taskService.deleteTask(2l);
        }
    );
    assertEquals(exception.getMessage(), "Error Tarea no disponible");
  }

  @Test
  void deleteException() {

    Throwable exception = assertThrows(
        NullPointerException.class, () -> {
          when(taskRepository.findById(null)).thenReturn(null);
          taskService.deleteTask(null);
          throw new NullPointerException("null");
        }
    );
    assertEquals(exception.getMessage(), "null");
  }

  @Test
  void getTaskById() {
    when(taskRepository.findById(1l)).thenReturn(Optional.ofNullable(task));
    TaskResponse response = taskService.getTaskById(1l);
    assertThat(response).isNotNull();
  }

  @Test
  void getTaskByIdNotFound() {
    when(taskRepository.findById(1l)).thenReturn(Optional.empty());
    Throwable exception = assertThrows(
        TaskNotFoundException.class, () -> {
          taskService.getTaskById(1l);
        }
    );
    assertEquals(exception.getMessage(), "Error Tarea no disponible");
  }
  @Test
  void getTaskException() {

    Throwable exception = assertThrows(
        NullPointerException.class, () -> {
          when(taskRepository.findById(null)).thenReturn(null);
          taskService.getTaskById(null);
          throw new NullPointerException("null");
        }
    );
    assertEquals(exception.getMessage(), "null");
  }
}