package com.soaint.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soaint.exception.TaskNotFoundException;
import com.soaint.service.TaskService;
import com.soaint.service.request.TaskRequest;
import com.soaint.service.response.BaseResponse;
import com.soaint.service.response.TaskResponse;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest
class TaskControllerTest {
  MockMvc mockMvc;
  @MockBean
  private TaskService taskService;
  @Autowired
  WebApplicationContext context;
  @Autowired
  private ObjectMapper objectMapper;
  TaskResponse taskResponse;
  TaskRequest taskRequest;

  @BeforeEach
  void setUp() {
    mockMvc = MockMvcBuilders
        .webAppContextSetup(context)
        .build();
    taskResponse = TaskResponse.builder()
        .descripcion("Tarea 1")
        .vigente(true)
        .build();

    taskRequest = new TaskRequest();
    taskRequest.setDescripcion("Tarea 1");
    taskRequest.setVigente(true);
  }

  @Test
  void getTaskById() throws Exception {
    given(taskService.getTaskById(1l))
        .willReturn(taskResponse);
    MvcResult response = mockMvc.perform(get("/api/v1/task/1"))
        .andExpect(status().isOk()).andReturn();
    //then
    assertEquals(200, response.getResponse().getStatus());
  }

  @Test
  void getTaskByIdNotFound() throws Exception {
    given(taskService.getTaskById(anyLong())).willThrow(new TaskNotFoundException("NOT FOUND"));
    MvcResult response = mockMvc.perform(get("/api/v1/task/0"))
        .andExpect(status().isNotFound())
        .andReturn();
    //then
    assertEquals(404, response.getResponse().getStatus());
  }

  @Test
  void getTask() throws Exception {
    given(taskService.getAll())
        .willReturn(Collections.singletonList(taskResponse));
    MvcResult response = mockMvc.perform(get("/api/v1/task"))
        .andExpect(status().isOk()).andReturn();
    //then
    assertEquals(200, response.getResponse().getStatus());
  }

  @Test
  void addTask() throws Exception {

    String jsonRequest = objectMapper.writeValueAsString(taskRequest);
    given(taskService.addTask(taskRequest))
        .willReturn(new BaseResponse("01", "Ok"));

    MvcResult response = mockMvc.perform(post("/api/v1/task")
            .content(jsonRequest)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();
    //then
    assertEquals(200, response.getResponse().getStatus());
  }

  @Test
  void addTaskBadRequest() throws Exception {
    taskRequest.setDescripcion("");
    String jsonRequest = objectMapper.writeValueAsString(taskRequest);
    given(taskService.addTask(taskRequest))
        .willReturn(new BaseResponse("01", "Ok"));

    MvcResult response = mockMvc.perform(post("/api/v1/task")
            .content(jsonRequest)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest()).andReturn();
    //then
    assertEquals(400, response.getResponse().getStatus());
  }

  @Test
  void editTask() throws Exception {

    String jsonRequest = objectMapper.writeValueAsString(taskRequest);
    given(taskService.editTask(taskRequest, 1l))
        .willReturn(new BaseResponse("01", "Ok"));

    MvcResult response = mockMvc.perform(put("/api/v1/task/1")
            .content(jsonRequest)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();
    //then
    assertEquals(200, response.getResponse().getStatus());
  }

  @Test
  void deleteTask() throws Exception {

    given(taskService.deleteTask(1l))
        .willReturn(new BaseResponse("01", "Ok"));

    MvcResult response = mockMvc.perform(delete("/api/v1/task/1")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();
    //then
    assertEquals(200, response.getResponse().getStatus());
  }
}