package com.soaint.domain;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@javax.persistence.Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_task")
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTask;

	private String descripcion;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;
	
	private boolean vigente;
}
