package com.soaint.service.request;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class TaskRequest {
	@NotBlank(message = "descripcion es obligatorio")
	private String descripcion;
	private boolean vigente;

}
