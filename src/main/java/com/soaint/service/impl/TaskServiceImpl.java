package com.soaint.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soaint.domain.Task;
import com.soaint.exception.TaskNotFoundException;
import com.soaint.repository.TaskRepository;
import com.soaint.service.TaskService;
import com.soaint.service.request.TaskRequest;
import com.soaint.service.response.BaseResponse;
import com.soaint.service.response.TaskResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TaskServiceImpl implements TaskService {

  @Autowired
  private TaskRepository taskRepository;

  @Override
  public List<TaskResponse> getAll() {
    List<TaskResponse> tasks = new ArrayList<>();
    try {
      taskRepository.findAll().forEach(x -> {
        TaskResponse taskResponse = TaskResponse.builder().idTask(x.getIdTask()).descripcion(x.getDescripcion())
            .vigente(x.isVigente()).build();
        tasks.add(taskResponse);
      });

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return tasks;
  }

  @Override
  public BaseResponse addTask(TaskRequest request) {
    BaseResponse response = new BaseResponse();
    try {
      Task task = Task.builder().descripcion(request.getDescripcion()).fechaCreacion(new Date()).vigente(true)
          .build();
      taskRepository.save(task);
      response.setCodigoRespuesta("01");
      response.setMensajeRespuesta("OK");
    } catch (Exception e) {
      response.setCodigoRespuesta("99");
      response.setMensajeRespuesta("Error al agregar");
      log.error(e.getMessage());
      e.printStackTrace();
    }
    return response;
  }

  @Override
  public BaseResponse editTask(TaskRequest request, Long idTask) {
    BaseResponse response = new BaseResponse();
    try {
      Optional<Task> taskFind = taskRepository.findById(idTask);
      if (taskFind.isPresent()) {
        taskFind.get().setDescripcion(request.getDescripcion());
        taskFind.get().setFechaModificacion(new Date());
        taskFind.get().setVigente(request.isVigente());
        taskRepository.save(taskFind.get());
        response.setCodigoRespuesta("01");
        response.setMensajeRespuesta("OK");
      } else {
        throw new TaskNotFoundException("Tarea no disponible");
      }
    } catch (TaskNotFoundException not) {
      throw new TaskNotFoundException("Error " + not.getMessage());
    } catch (Exception e) {
      response.setCodigoRespuesta("99");
      response.setMensajeRespuesta("Error al actualizar " + e.getMessage());
      log.error(e.getMessage());
      e.printStackTrace();
    }
    return response;
  }

  @Override
  public BaseResponse deleteTask(Long idTask) {
    BaseResponse response = new BaseResponse();
    try {
      Optional<Task> taskFind = taskRepository.findById(idTask);
      if (taskFind.isPresent()) {
        // eliminacion fisica
        // taskRepository.deleteById(idTask);

        // Eliminacion logica
        taskFind.get().setVigente(false);
        taskRepository.save(taskFind.get());

        response.setCodigoRespuesta("01");
        response.setMensajeRespuesta("OK");
      } else {
        throw new TaskNotFoundException("Tarea no disponible");
      }
    } catch (TaskNotFoundException not) {
      throw new TaskNotFoundException("Error " + not.getMessage());
    } catch (Exception e) {
      response.setCodigoRespuesta("99");
      response.setMensajeRespuesta("Error al Eliminar");
      log.error(e.getMessage());
      e.printStackTrace();
    }
    return response;
  }

  @Override
  public TaskResponse getTaskById(Long idTask) {
    TaskResponse response = new TaskResponse();
    try {
      Optional<Task> taskFind = taskRepository.findById(idTask);
      if (taskFind.isPresent()) {
        response = TaskResponse.builder().idTask(taskFind.get().getIdTask())
            .descripcion(taskFind.get().getDescripcion()).vigente(taskFind.get().isVigente()).build();
      } else {
        throw new TaskNotFoundException("Tarea no disponible");
      }
    } catch (TaskNotFoundException not) {
      throw new TaskNotFoundException("Error " + not.getMessage());
    } catch (Exception e) {
      log.error(e.getMessage());
      e.printStackTrace();
    }

    return response;
  }

}
