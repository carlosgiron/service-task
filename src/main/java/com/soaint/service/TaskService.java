package com.soaint.service;

import java.util.List;

import com.soaint.service.request.TaskRequest;
import com.soaint.service.response.BaseResponse;
import com.soaint.service.response.TaskResponse;

public interface TaskService {

	TaskResponse getTaskById(Long idTask);

	List<TaskResponse> getAll();

	BaseResponse addTask(TaskRequest request);

	BaseResponse editTask(TaskRequest request, Long idTask);

	BaseResponse deleteTask(Long idTask);

}
