package com.soaint.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
public class WebConfig {
	@Value("#{environment['soaint.cors.origins']}")
	private String origins;

	@Value("#{environment['soaint.cors.headers']}")
	private String headers;

	@Value("#{environment['soaint.cors.methods']}")
	private String methods;
	
	 

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList(origins.split(",")));
		configuration.setAllowedHeaders(Arrays.asList(headers.split(",")));
		configuration.setAllowedMethods(Arrays.asList(methods.split(",")));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}
