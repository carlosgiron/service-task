package com.soaint.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.soaint.domain.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

}
