package com.soaint.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soaint.service.TaskService;
import com.soaint.service.request.TaskRequest;
import com.soaint.service.response.BaseResponse;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/task")
public class TaskController {

  @Autowired
  private TaskService taskService;

  @GetMapping("/{idTask}")
  @Operation(summary = "Lista tarea por id")
  public ResponseEntity<?> getTaskById(@PathVariable(value = "idTask") Long idTask) {
    return ResponseEntity.ok(taskService.getTaskById(idTask));
  }

  @GetMapping
  @Operation(summary = "Lista todas las tareas")
  public ResponseEntity<?> getTask() {
    return ResponseEntity.ok(taskService.getAll());
  }

  @PostMapping
  @Operation(summary = "Inserta tareas")
  public ResponseEntity<BaseResponse> addTask(@RequestBody @Valid TaskRequest request) {
    BaseResponse response = taskService.addTask(request);
    return ResponseEntity.ok(response);
  }

  @PutMapping("/{idTask}")
  @Operation(summary = "actualiza tareas por id")
  public ResponseEntity<BaseResponse> editTask(@RequestBody @Valid TaskRequest request,
                                               @PathVariable(value = "idTask") Long idTask) {
    return ResponseEntity.ok(taskService.editTask(request, idTask));
  }

  @DeleteMapping("/{idTask}")
  @Operation(summary = "Eliminacion logica de tarea")
  public ResponseEntity<BaseResponse> deleteTask(@PathVariable(value = "idTask") Long idTask) {

    BaseResponse response = taskService.deleteTask(idTask);

    return ResponseEntity.ok(response);
  }
}
